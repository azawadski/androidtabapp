package layout;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.example.alexander.tabapp.DBManager;
import com.example.alexander.tabapp.R;

/**
 * Add Fragment aims to:
 * Create a seperate fragment for the 'Add' tab
 * Allows user to write and add entries to a Database
 * Displays the List from the Database created in a List View
 */
public class AddFragment extends Fragment
{
    public static EditText textInput;
    ListView addItemListView;
    SimpleCursorAdapter simpleCursorAdapter;            //An adapter is needed to communicate with the Database and ListView
    DBManager dbManager;                                //An instance of the Database Manager created so we can interact with the Database

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //To make Fragments show up you need to 'Inflate' them
        View rootView = inflater.inflate(R.layout.fragment_add, container, false);

        textInput = (EditText) rootView.findViewById(R.id.textInput);
        addItemListView = (ListView) rootView.findViewById(R.id.addListView);
        final Button addButton = (Button) rootView.findViewById(R.id.addButton);
        final Button addUpdateButton = (Button) rootView.findViewById(R.id.addUpdateButton);

        dbManager = new DBManager(getContext(), null, null, 1);     //Instantiating the Database

        //For Fragments you need to create 'Listeners' to know when an item (Buttons in this case) have been interacted with, you call the function within the Listener
        addButton.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View v)
                    {
                        addButtonClicked(v);
                    }
                }
        );

        addUpdateButton.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View v)
                    {
                        displayItemList();
                    }
                }
        );
        displayItemList();
        return rootView;
    }

    //Adds items to the Database
    public void addButtonClicked(View view)
    {
        dbManager.addItem(textInput.getText().toString());
        displayItemList();
    }

    //Updates the List View - done by using an Adapater between the Cursor which points to a location in the Database and List View items
    public void displayItemList()
    {
            Cursor cursor = dbManager.getAllItems();
            if (cursor.getCount() == 0)
            {
                return;
            }
            String[] columns = new String[]{dbManager.COLUMN1_ID, dbManager.COLUMN2_ITEM_NAME};
            int[] boundTo = new int[] {R.id.itemID, R.id.itemName};

            //list_view.xml Layout is used here as a layout formatting for all the List View items - you can create custom list view layouts
            simpleCursorAdapter = new SimpleCursorAdapter(this.getActivity(), R.layout.list_view, cursor, columns, boundTo, 0);
            addItemListView.setAdapter(simpleCursorAdapter);
    }

    //Testing Function to delete the Database
    /*public void deleteButtonClicked(View view)
    {
        getContext().deleteDatabase("itemlist.db");
        displayItemList();
    }*/
}
