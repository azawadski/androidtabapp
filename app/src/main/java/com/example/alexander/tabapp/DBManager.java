package com.example.alexander.tabapp;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;

/**
 * Responsible for Managing the 'itemlist.db' Database with SQLite
 * Call and create methods within this to interact with the database
 */

public class DBManager extends SQLiteOpenHelper
{
    //Setting up the Parameters for the Database
    private static final int DATABASE_VERSION = 1;                //Any changes to the Stored values will need to be updated
    private static final String DATABASE_NAME = "itemlist.db";    //Name of the Database
    public static final String TABLENAME_ITEM = "items";          //Name of one of the possible tables within the Database (NOTE: DO NOT use TABLE_NAME as a declaration - SQL hates it)
    public static final String COLUMN1_ID = "_id";                //Column 1 contains the _id  (underscore is requirement)
    public static final String COLUMN2_ITEM_NAME = "itemName";    //Column 2 contains the item's Name

    //Constructor
    public DBManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        //CREATE TABLE items(_id INTEGER PRIMARY KEY AUTOINCREMENT, itemName TEXT);
        String createQuery = "CREATE TABLE " + TABLENAME_ITEM  + "(" +
                COLUMN1_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN2_ITEM_NAME + " TEXT " +
                ");";
        db.execSQL(createQuery);
        db.execSQL("INSERT INTO items (_id, itemName) VALUES (0, 'Bacon');");
        db.execSQL("INSERT INTO items (_id, itemName) VALUES (1, 'Eggs');");
        db.execSQL("INSERT INTO items (_id, itemName) VALUES (7, 'Toast');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL( "DROP TABLE IF EXISTS " + TABLENAME_ITEM  );    //Delete if a Database Exists
        onCreate(db);
    }

    //Add a new item/Row
    public void addItem(String itemName)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN2_ITEM_NAME, itemName);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLENAME_ITEM , null, values);
        db.close();
    }

    //Delete an item from id passed into it
    public void deleteItem(long itemID)
    {
        //You will need to fill this in yourself :^)
    }

    //Gets all the items requied for displaying in List Views in Add and Delete Fragments
    public Cursor getAllItems()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLENAME_ITEM, new String[] {COLUMN1_ID, COLUMN2_ITEM_NAME}, null, null, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
            return cursor;
        }
        else
        {
            return null;
        }
    }
}
